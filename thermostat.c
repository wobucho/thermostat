
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "driverlib/flash.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "utils/swupdate.h"
#include "utils/ustdlib.h"
#include "utils/uartstdio.h"
#include "drivers/pinout.h"
#include "config.h"
#include "lwip_task.h"
#include "serial_task.h"
#include "serial.h"
#include "send_command.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

//*****************************************************************************
//
// The current IP address.
//
//*****************************************************************************
uint32_t g_ui32IPAddress;

//*****************************************************************************
//
// The system clock frequency.
//
//*****************************************************************************
uint32_t g_ui32SysClock;

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif

//*****************************************************************************
//
// This hook is called by FreeRTOS when an stack overflow error is detected.
//
//*****************************************************************************
void
vApplicationStackOverflowHook(xTaskHandle *pxTask, signed char *pcTaskName)
{
    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// The main entry function which configures the clock and hardware peripherals
// needed by the S2E application before calling the FreeRTOS scheduler.
//
//*****************************************************************************
int
main(void)
{
    //
    // Run from the PLL at configCPU_CLOCK_HZ MHz.
    //
    g_ui32SysClock = MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                                             SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
                                             SYSCTL_CFG_VCO_480),
                                            configCPU_CLOCK_HZ);

    //
    // Configure the device pins based on the selected board.
    //
#ifndef DK_TM4C129X
    PinoutSet(true, false);
#else
    PinoutSet();
#endif

    //
    // Configure Debug UART.
    //
    UARTStdioConfig(2, 115200, g_ui32SysClock);

    //
    // Clear the terminal and print banner.
    //
    UARTprintf("\033[2J\033[H");
    UARTprintf("Sinara Thermostat\n\n");

    //
    // Tell the user what we are doing now.
    //
    UARTprintf("Waiting for IP.\n");

    //
    // Initialize the configuration parameter module.
    //
    ConfigInit();

    //
    // Initialize the Ethernet peripheral and create the lwIP tasks.
    //
    if(lwIPTaskInit() != 0)
    {
        UARTprintf("Failed to create lwIP tasks!\n");
        while(1)
        {
        }
    }

    //
    // Initialize the serial peripherals and create the Serial task.
    //
    if(SerialTaskInit() != 0)
    {
        UARTprintf("Failed to create Serial task!\n");
        while(1)
        {
        }
    }

    //
    // Start the scheduler.  This should not return.
    //
    vTaskStartScheduler();

    //
    // In case the scheduler returns for some reason, print an error and loop
    // forever.
    //
    UARTprintf("\nScheduler returned unexpectedly!\n");
    while(1)
    {
    }
}
