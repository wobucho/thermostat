################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
EXE_SRCS += \
../makefsfile.exe 

CMD_SRCS += \
../thermostat_ccs.cmd 

C_SRCS += \
../config.c \
../enet_fs.c \
../idle_task.c \
../lwip_task.c \
../send_command.c \
../serial.c \
../serial_task.c \
../startup_ccs.c \
../telnet.c \
../thermostat.c 

C_DEPS += \
./config.d \
./enet_fs.d \
./idle_task.d \
./lwip_task.d \
./send_command.d \
./serial.d \
./serial_task.d \
./startup_ccs.d \
./telnet.d \
./thermostat.d 

OBJS += \
./config.obj \
./enet_fs.obj \
./idle_task.obj \
./lwip_task.obj \
./send_command.obj \
./serial.obj \
./serial_task.obj \
./startup_ccs.obj \
./telnet.obj \
./thermostat.obj 

OBJS__QUOTED += \
"config.obj" \
"enet_fs.obj" \
"idle_task.obj" \
"lwip_task.obj" \
"send_command.obj" \
"serial.obj" \
"serial_task.obj" \
"startup_ccs.obj" \
"telnet.obj" \
"thermostat.obj" 

C_DEPS__QUOTED += \
"config.d" \
"enet_fs.d" \
"idle_task.d" \
"lwip_task.d" \
"send_command.d" \
"serial.d" \
"serial_task.d" \
"startup_ccs.d" \
"telnet.d" \
"thermostat.d" 

C_SRCS__QUOTED += \
"../config.c" \
"../enet_fs.c" \
"../idle_task.c" \
"../lwip_task.c" \
"../send_command.c" \
"../serial.c" \
"../serial_task.c" \
"../startup_ccs.c" \
"../telnet.c" \
"../thermostat.c" 

EXE_SRCS__QUOTED += \
"../makefsfile.exe" 


