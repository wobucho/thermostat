/*
 * send_command.c
 *
 *
 */


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/ringbuf.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "config.h"
#include "serial.h"
#include "send_command.h"

//*****************************************************************************
//
// External References.
//
//*****************************************************************************
extern uint32_t g_ui32SysClock;

//*****************************************************************************
//
//! \addtogroup serial_api
//! @{
//
//*****************************************************************************

//*****************************************************************************
//

void Send_Command(uint32_t ui32Port, const char *commandBuf, uint32_t ui32Len)
{

    ASSERT((ui32Port == 0) || (ui32Port == 1));

    unsigned int charNum = 0;
    while(charNum < ui32Len)
    {
        if(!SerialSendFull(ui32Port))
        {
            SerialSend(ui32Port, commandBuf[charNum]);
            charNum = charNum + 1;
        }
    }
    if(!SerialSendFull(ui32Port)) SerialSend(ui32Port, '\r');
    if(!SerialSendFull(ui32Port)) SerialSend(ui32Port, '\n');
}

uint32_t Receive_Answer(uint32_t ui32Port, char *answerBuf)
{

    ASSERT((ui32Port == 0) || (ui32Port == 1));

    unsigned int charNum = 0;
    int32_t buf;
    int8_t cbuf;
    int i = 0;
    int imax = 10000000; // timeout about 3 seconds
    bool timein = true;

    while(timein)
    {
        cbuf = -1;

        if(ui32Port == 0)
        {
            while(!UARTCharsAvail(S2E_PORT0_UART_PORT) && timein)
            {
                if(i > imax)
                {
                    timein = false;
                    charNum = 0xFF;
                }
                i = i + 1;
            }
            if(UARTCharsAvail(S2E_PORT0_UART_PORT))
            {
                buf = MAP_UARTCharGetNonBlocking(S2E_PORT0_UART_PORT);
                cbuf = (unsigned char)(buf & 0xFF);
            }
        } else
        {
            while(!UARTCharsAvail(S2E_PORT1_UART_PORT) && timein)
            {
                if(i > imax)
                {
                    timein = false;
                    charNum = 0xFF;
                }
                i = i + 1;
            }
            if(UARTCharsAvail(S2E_PORT1_UART_PORT))
            {
                buf = MAP_UARTCharGetNonBlocking(S2E_PORT1_UART_PORT);
                cbuf = (unsigned char)(buf & 0xFF);
            }
        }

        if(cbuf != -1)
        {
            if((cbuf == '\r') || (cbuf == '\n') || (cbuf == 0x1b))
            {
                answerBuf[charNum] = cbuf;
                break;
            }
            else
            {
                answerBuf[charNum] = cbuf;
                charNum = charNum + 1;
            }
        }
    }

    if(charNum == 0xFF)
    {
        answerBuf[0] = 't';
        answerBuf[1] = 'i';
        answerBuf[2] = 'm';
        answerBuf[3] = 'e';
        answerBuf[4] = 'o';
        answerBuf[5] = 'u';
        answerBuf[6] = 't';
        answerBuf[7] = '\n';
        charNum = 8;
    }

    return charNum;
}

uint32_t General_Command(uint32_t ui32Port, uint32_t mode, char *answerBuf)
{

    ASSERT((mode == 0) || (mode == 1) || (mode == 2) || (mode == 3));

    char command[2];
    uint32_t ans_len = 0;

    switch(mode)
    {
    case 0:
        command[0] = 'm'; //reads the version of hardware and software
        break;
    case 1:
        command[0] = 'u'; //reads the UUID of the MTD415T
        break;
    case 2:
        command[0] = 'E'; //reads the Error Register
        break;
    case 3:
        command[0] = 'c'; //resets the Error register
        break;
    }

    command[1] = '?';

    if(mode == 3)
    {
        Send_Command(ui32Port,command,1);
    } else
    {
        Send_Command(ui32Port,command,2);
        ans_len = Receive_Answer(ui32Port,answerBuf);
    }

    return ans_len;
}

bool Set_TEC_limit(uint32_t ui32Port, uint32_t limit)
{

    char command[5];
    command[0] = 'L';

    if(limit >= 200 && limit < 1000)
    {
        command[1] = ((limit/100) % 10) + '0';
        command[2] = ((limit/10) % 10) + '0';
        command[3] = ((limit) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(limit >= 1000 && limit <= 2000)
    {
        command[1] = ((limit/1000) % 10) + '0';
        command[2] = ((limit/100) % 10) + '0';
        command[3] = ((limit/10) % 10) + '0';
        command[4] = ((limit) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    }  else
    {
        return false;
    }
}

uint32_t Read_TEC(uint32_t ui32Port, uint32_t mode, char *answerBuf)
{
    ASSERT((mode == 0) || (mode == 1) || (mode == 2));

    char command[2];
    uint32_t ans_len;

    switch(mode)
    {
    case 0:
        command[0] = 'L'; //reads the TEC current limit
        break;
    case 1:
        command[0] = 'A'; //reads the actual TEC current
        break;
    case 2:
        command[0] = 'U'; //reads the actual TEC voltage
        break;
    }

    command[1] = '?';

    Send_Command(ui32Port,command,2);
    ans_len = Receive_Answer(ui32Port,answerBuf);

    return ans_len;
}

bool Set_Temp(uint32_t ui32Port, uint32_t temp)
{

    char command[6];
    command[0] = 'T';

    if(temp >= 5000 && temp < 10000)
    {
        command[1] = ((temp/1000) % 10) + '0';
        command[2] = ((temp/100) % 10) + '0';
        command[3] = ((temp/10) % 10) + '0';
        command[4] = ((temp) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(temp >= 10000 && temp <= 45000)
    {
        command[1] = ((temp/10000) % 10) + '0';
        command[2] = ((temp/1000) % 10) + '0';
        command[3] = ((temp/100) % 10) + '0';
        command[4] = ((temp/10) % 10) + '0';
        command[5] = ((temp) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    }  else
    {
        return false;
    }
}

bool Set_Temp_Window(uint32_t ui32Port, uint32_t temp_window)
{

    char command[6];
    command[0] = 'W';

    if(temp_window >= 1 && temp_window < 10)
    {
        command[1] = ((temp_window) % 10) + '0';
        Send_Command(ui32Port,command,2);
        return true;
    } else if(temp_window >= 10 && temp_window < 100 )
    {
        command[1] = ((temp_window/10) % 10) + '0';
        command[2] = ((temp_window) % 10) + '0';
        Send_Command(ui32Port,command,3);
        return true;
    }  else if(temp_window >= 100 && temp_window < 1000 )
    {
        command[1] = ((temp_window/100) % 10) + '0';
        command[2] = ((temp_window/10) % 10) + '0';
        command[3] = ((temp_window) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(temp_window >= 1000 && temp_window < 10000 )
    {
        command[1] = ((temp_window/1000) % 10) + '0';
        command[2] = ((temp_window/100) % 10) + '0';
        command[3] = ((temp_window/10) % 10) + '0';
        command[4] = ((temp_window) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(temp_window >= 10000 && temp_window <= 32768)
    {
        command[1] = ((temp_window/10000) % 10) + '0';
        command[2] = ((temp_window/1000) % 10) + '0';
        command[3] = ((temp_window/100) % 10) + '0';
        command[4] = ((temp_window/10) % 10) + '0';
        command[5] = ((temp_window) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    } else
    {
        return false;
    }
}

bool Set_Temp_Delay(uint32_t ui32Port, uint32_t temp_delay)
{

    char command[6];
    command[0] = 'd';

    if(temp_delay >= 1 && temp_delay < 10)
    {
        command[1] = ((temp_delay) % 10) + '0';
        Send_Command(ui32Port,command,2);
        return true;
    } else if(temp_delay >= 10 && temp_delay < 100 )
    {
        command[1] = ((temp_delay/10) % 10) + '0';
        command[2] = ((temp_delay) % 10) + '0';
        Send_Command(ui32Port,command,3);
        return true;
    }  else if(temp_delay >= 100 && temp_delay < 1000 )
    {
        command[1] = ((temp_delay/100) % 10) + '0';
        command[2] = ((temp_delay/10) % 10) + '0';
        command[3] = ((temp_delay) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(temp_delay >= 1000 && temp_delay < 10000 )
    {
        command[1] = ((temp_delay/1000) % 10) + '0';
        command[2] = ((temp_delay/100) % 10) + '0';
        command[3] = ((temp_delay/10) % 10) + '0';
        command[4] = ((temp_delay) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(temp_delay >= 10000 && temp_delay <= 32768)
    {
        command[1] = ((temp_delay/10000) % 10) + '0';
        command[2] = ((temp_delay/1000) % 10) + '0';
        command[3] = ((temp_delay/100) % 10) + '0';
        command[4] = ((temp_delay/10) % 10) + '0';
        command[5] = ((temp_delay) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    } else
    {
        return false;
    }
}

uint32_t Read_Temp(uint32_t ui32Port, uint32_t mode, char *answerBuf)
{
    ASSERT((mode == 0) || (mode == 1) || (mode == 2) || (mode == 3));

    char command[3];
    uint32_t ans_len;

    switch(mode)
    {
    case 0:
        command[0] = 'T'; //reads the set temperature
        break;
    case 1:
        command[0] = 'T'; //reads the actual temperature
        command[1] = 'e';
        break;
    case 2:
        command[0] = 'W'; //reads the temperature window
        break;
    case 3:
        command[0] = 'd'; //reads the delay time between reaching the temperature window and activating the Status output pin
        break;
    }

    if(mode == 1)
    {
        command[2] = '?';
        Send_Command(ui32Port,command,3);
        ans_len = Receive_Answer(ui32Port,answerBuf);
    } else
    {
        command[1] = '?';
        Send_Command(ui32Port,command,2);
        ans_len = Receive_Answer(ui32Port,answerBuf);
    }

    return ans_len;
}

bool Set_Gain(uint32_t ui32Port, uint32_t gain)
{

    char command[7];
    command[0] = 'G';

    if(gain >= 10 && gain < 100 )
    {
        command[1] = ((gain/10) % 10) + '0';
        command[2] = ((gain) % 10) + '0';
        Send_Command(ui32Port,command,3);
        return true;
    }  else if(gain >= 100 && gain < 1000 )
    {
        command[1] = ((gain/100) % 10) + '0';
        command[2] = ((gain/10) % 10) + '0';
        command[3] = ((gain) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(gain >= 1000 && gain < 10000 )
    {
        command[1] = ((gain/1000) % 10) + '0';
        command[2] = ((gain/100) % 10) + '0';
        command[3] = ((gain/10) % 10) + '0';
        command[4] = ((gain) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(gain >= 10000 && gain < 100000)
    {
        command[1] = ((gain/10000) % 10) + '0';
        command[2] = ((gain/1000) % 10) + '0';
        command[3] = ((gain/100) % 10) + '0';
        command[4] = ((gain/10) % 10) + '0';
        command[5] = ((gain) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    } else if(gain == 100000)
    {
        command[1] = '1';
        command[2] = '0';
        command[3] = '0';
        command[4] = '0';
        command[5] = '0';
        command[6] = '0';
        Send_Command(ui32Port,command,7);
        return true;
    } else
    {
        return false;
    }
}

bool Set_Period(uint32_t ui32Port, uint32_t period)
{

    char command[7];
    command[0] = 'O';

    if(period >= 100 && period < 1000 )
    {
        command[1] = ((period/100) % 10) + '0';
        command[2] = ((period/10) % 10) + '0';
        command[3] = ((period) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(period >= 1000 && period < 10000 )
    {
        command[1] = ((period/1000) % 10) + '0';
        command[2] = ((period/100) % 10) + '0';
        command[3] = ((period/10) % 10) + '0';
        command[4] = ((period) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(period >= 10000 && period < 100000)
    {
        command[1] = ((period/10000) % 10) + '0';
        command[2] = ((period/1000) % 10) + '0';
        command[3] = ((period/100) % 10) + '0';
        command[4] = ((period/10) % 10) + '0';
        command[5] = ((period) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    } else if(period == 100000)
    {
        command[1] = '1';
        command[2] = '0';
        command[3] = '0';
        command[4] = '0';
        command[5] = '0';
        command[6] = '0';
        Send_Command(ui32Port,command,7);
        return true;
    } else
    {
        return false;
    }
}

bool Set_CT(uint32_t ui32Port, uint32_t ct)
{

    char command[5];
    command[0] = 'C';

    if(ct >= 1 && ct < 10 )
    {
        command[1] = ((ct) % 10) + '0';
        Send_Command(ui32Port,command,2);
        return true;
    } else if(ct >= 10 && ct < 100 )
    {
        command[1] = ((ct/10) % 10) + '0';
        command[2] = ((ct) % 10) + '0';
        Send_Command(ui32Port,command,3);
        return true;
    } else if(ct >= 100 && ct < 1000)
    {
        command[1] = ((ct/100) % 10) + '0';
        command[2] = ((ct/10) % 10) + '0';
        command[3] = ((ct) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(ct == 1000)
    {
        command[1] = '1';
        command[2] = '0';
        command[3] = '0';
        command[4] = '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else
    {
        return false;
    }
}

bool Set_PID(uint32_t ui32Port, uint32_t mode, int32_t pid)
{

    char command[7];

    switch(mode)
    {
    case 0:
        command[0] = 'P'; //sets the P Share
        break;
    case 1:
        command[0] = 'I'; //sets the I Share
        break;
    case 2:
        command[0] = 'D'; //sets the D Share
        break;
    }

    if(pid >= 0 && pid < 10 )
    {
        command[1] = ((pid) % 10) + '0';
        Send_Command(ui32Port,command,2);
        return true;
    } else if(pid >= 10 && pid < 100 )
    {
        command[1] = ((pid/10) % 10) + '0';
        command[2] = ((pid) % 10) + '0';
        Send_Command(ui32Port,command,3);
        return true;
    }  else if(pid >= 100 && pid < 1000 )
    {
        command[1] = ((pid/100) % 10) + '0';
        command[2] = ((pid/10) % 10) + '0';
        command[3] = ((pid) % 10) + '0';
        Send_Command(ui32Port,command,4);
        return true;
    } else if(pid >= 1000 && pid < 10000 )
    {
        command[1] = ((pid/1000) % 10) + '0';
        command[2] = ((pid/100) % 10) + '0';
        command[3] = ((pid/10) % 10) + '0';
        command[4] = ((pid) % 10) + '0';
        Send_Command(ui32Port,command,5);
        return true;
    } else if(pid >= 10000 && pid < 100000)
    {
        command[1] = ((pid/10000) % 10) + '0';
        command[2] = ((pid/1000) % 10) + '0';
        command[3] = ((pid/100) % 10) + '0';
        command[4] = ((pid/10) % 10) + '0';
        command[5] = ((pid) % 10) + '0';
        Send_Command(ui32Port,command,6);
        return true;
    } else if(pid == 100000)
    {
        command[1] = '1';
        command[2] = '0';
        command[3] = '0';
        command[4] = '0';
        command[5] = '0';
        command[6] = '0';
        Send_Command(ui32Port,command,7);
        return true;
    } else
    {
        return false;
    }
}

uint32_t Read_Control_Loop(uint32_t ui32Port, uint32_t mode, char *answerBuf)
{
    ASSERT((mode == 0) || (mode == 1) || (mode == 2) || (mode == 3) || (mode == 4) || (mode == 5));

    char command[2];
    uint32_t ans_len;

    switch(mode)
    {
    case 0:
        command[0] = 'G'; //reads the critical gain
        break;
    case 1:
        command[0] = 'O'; //reads the critical period
        break;
    case 2:
        command[0] = 'C'; //reads the cycling time
        break;
    case 3:
        command[0] = 'P'; //reads the P Share
        break;
    case 4:
        command[0] = 'I'; //reads the I Share
        break;
    case 5:
        command[0] = 'D'; //reads the D Share
        break;
    }

    command[1] = '?';
    Send_Command(ui32Port,command,2);
    ans_len = Receive_Answer(ui32Port,answerBuf);

    return ans_len;
}

