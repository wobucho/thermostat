/*
 * send_command.h
 *
 *
 */

#ifndef SEND_COMMAND_H_
#define SEND_COMMAND_H_


extern void Send_Command(uint32_t ui32Port, const char *commandBuf, uint32_t ui32Len);
extern uint32_t Receive_Answer(uint32_t ui32Port, char *answerBuf);
uint32_t General_Command(uint32_t ui32Port, uint32_t mode, char *answerBuf);
bool Set_TEC_limit(uint32_t ui32Port, uint32_t limit);
uint32_t Read_TEC(uint32_t ui32Port, uint32_t mode, char *answerBuf);
bool Set_Temp(uint32_t ui32Port, uint32_t temp);
bool Set_Temp_Window(uint32_t ui32Port, uint32_t temp_window);
bool Set_Temp_Delay(uint32_t ui32Port, uint32_t temp_delay);
uint32_t Read_Temp(uint32_t ui32Port, uint32_t mode, char *answerBuf);
bool Set_Gain(uint32_t ui32Port, uint32_t gain);
bool Set_Period(uint32_t ui32Port, uint32_t period);
bool Set_CT(uint32_t ui32Port, uint32_t ct);
bool Set_PID(uint32_t ui32Port, uint32_t mode, int32_t pid);
uint32_t Read_Control_Loop(uint32_t ui32Port, uint32_t mode, char *answerBuf);

#endif /* SEND_COMMAND_H_ */
