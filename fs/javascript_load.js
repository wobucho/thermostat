<!-- Copyright (c) 2014-2015 Texas Instruments Incorporated.  All rights reserved. -->

window.onload = function()
{
    document.getElementById('config0').onclick = loadConfig0;
    document.getElementById('config1').onclick = loadConfig1;
    document.getElementById('misc').onclick = loadMisc;

    loadPage("about.htm");
}
